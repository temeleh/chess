package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * Ohjelman pääluokka.
 */
public class Launch extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        BorderPane root = FXMLLoader.load(getClass().getResource("gui.fxml"));

        primaryStage.setTitle("Chess");
        primaryStage.getIcons().add(new Image("/textures/BoardTiles.jpg"));
        primaryStage.setScene(new Scene(root, root.getPrefWidth(), root.getPrefHeight()));
        primaryStage.setMaximized(true);
        primaryStage.show();

        // asetetaan fokus root elementtiin, jotta se voi hallita nappaimiston painallukset
        root.requestFocus();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
