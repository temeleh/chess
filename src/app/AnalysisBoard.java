package app;

import app.utils.Tile;

/**
 * Luokka mallintaa shakkilautaa, jolla voidaan testata siirtoja.
 */
public class AnalysisBoard extends Board {

    /**
     * Luo uuden analyysilaudan parametrina annetun laudan avulla.
     * Analyysilaudan nappuloille ei generoida mesh dataa.
     *
     * @param board lauta, jonka tila otetaan analyysilaudan tilaksi
     */
    public AnalysisBoard(Board board) {
        super(false);

        this.currentState = copyState(board.currentState);
        this.initialState = copyState(this.currentState);
        this.turn = board.turn;
    }

    /**
     * Tekee siirron vastaavalle analyysilaudan nappulalle, joka on parametrina saadun nappulan koordinaateissa.
     * Analyysisiirto ei generoi sallittuja siirtoja nappuloille siirron jalkeen.
     *
     * @param from pelilaudan nappula, jota vastaavaa analyysilaudan nappulaa siirretaan.
     * @param target tile, johon nappula siirretaan
     */
    public void makeAnalysisMove(Chessman from, Tile target) {
        super.makeMove(getChessman(from.getTile()), target, null, false);
    }

    /**
     * Tarkistaa onko omaan, eli siihen kuninkaaseen, jonka vuoro on talla hetkella hyokkayksia.
     *
     * @return true, jos kuningas on shakissa
     */
    public boolean isKingInCheck() {
        return isTileAttacked(getOwnKing().getTile());
    }

    /**
     * Palauttaa laudan takaisin alkutilaan, eli tilaan jolla analyysilauta luotiin.
     * Vuoronumeroa ei kuitenkaan palauteta.
     */
    @Override
    public void reset() {
	    setState(this.initialState);
    }


    /**
     * Palauttaa laudan edelliseen tilaan, jos edellinen tila on tallennettuna
     */
    @Override
    public void undo() {
        if (!previousStates.isEmpty()) {
            turn -= 1;
            setState(previousStates.get(previousStates.size() - 1));

            removePreviousState();
        }
    }
}
