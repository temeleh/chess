package app.ai;


import app.AnalysisBoard;
import app.Board;
import app.Chessman;
import app.utils.Move;
import app.utils.Tile;

import java.util.ArrayList;
import java.util.List;

public class SimpleEngine {

    private Board board;

    public SimpleEngine(Board board) {
        this.board = board;
    }

    private int getTileValue(Chessman chessman) {
        if (chessman == null) {
            return 0;
        }
        return isOwnChessman(chessman) ? -chessman.getType().getValue() : chessman.getType().getValue();
    }

    private boolean isOwnChessman(Chessman chessman) {
        return board.isWhiteTurn() == chessman.isWhite();
    }


    /**
     * Palauttaa tekoalyn siirron eli liikutettavan nappulan, siirtoindeksin ja siirron arvon
     *
     * @param depth haun syvyys
     * @return tekoalyn tekeman siirron
     */
    public Move getBestMove(int depth) {
        AnalysisBoard aBoard = new AnalysisBoard(board);

        List<Move> moves = new ArrayList<>();
        Move bestMove = null;

        try {
            for (Chessman chessman : board.getState()) {
                if (!chessman.isAlive() || chessman.isWhite() != board.isWhiteTurn()) continue;

                List<Tile> allowedTiles = chessman.getAllowedTiles();

                for (Tile currentTile : allowedTiles) {
                    Move boardMove = new Move(chessman, currentTile);
                    Move currentMove = new Move(aBoard.getChessman(boardMove.getChessman().getTile()), currentTile);

                    int promoteGain = 0;
                    if (currentMove.isPromoting()) {
                        boardMove.setPromoteType(Chessman.Type.QUEEN);
                        currentMove.setPromoteType(Chessman.Type.QUEEN);
                        promoteGain = 8;
                    }

                    boardMove.setScore(getMoveScore(currentMove, depth, aBoard) +
                            getTileValue(board.getChessman(currentTile)) + promoteGain);

                    moves.add(boardMove);

                    if (bestMove == null || boardMove.getScore() > bestMove.getScore()) {
                        bestMove = boardMove;
                    }
                    System.out.println(boardMove);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("\n\n---------\n\n");

        for (int i = moves.size() - 1; i >= 0; i--) {
            if (moves.get(i).getScore() < bestMove.getScore()) {
                moves.remove(i);
            }
        }

        //get random allowed move
        if (moves.size() > 1) {
            bestMove = moves.get((int) (Math.random() * moves.size()));
        }

        return bestMove;
    }

    private int getMoveScore(Move move, int depth, AnalysisBoard aBoard) {
        if (--depth <= 0) {
            return 0;
        }

        aBoard.makeMove(move, true);

//        StringBuilder sb = new StringBuilder();
//        for (int i = 0; i < depth; i++) {
//            sb.append("\t");
//        }
//
//        System.out.println(sb.toString() + (board.isWhiteTurn() == aBoard.isWhiteTurn()));

        // is stalemate or checkmate
        boolean isGameEndingMove = true;

        int score = board.isWhiteTurn() == aBoard.isWhiteTurn() ? -1000000 : 1000000;

        for (Chessman chessman : aBoard.getState()) {
            if (!chessman.isAlive() || chessman.isWhite() != aBoard.isWhiteTurn()) continue;

            List<Tile> allowedTiles = chessman.getAllowedTiles();

            if (allowedTiles.isEmpty()) continue;

            isGameEndingMove = false;

            for (Tile currentTile : allowedTiles) {
                Move currentMove = new Move(chessman, currentTile);

                int promoteGain = 0;
                if (currentMove.isPromoting()) {
                    currentMove.setPromoteType(Chessman.Type.QUEEN);
                    promoteGain = isOwnChessman(chessman) ? 8 : -8;
                }

                currentMove.setScore(getMoveScore(currentMove, depth, aBoard) +
                        getTileValue(aBoard.getChessman(currentTile)) + promoteGain);

                //System.out.println(sb.toString() + currentMove + "    is own turn: " + (board.isWhiteTurn() == chessman.isWhite()));

                if (isOwnChessman(chessman)) {
                    if (currentMove.getScore() > score)
                        score = currentMove.getScore();

                } else {
                    if (currentMove.getScore() < score)
                        score = currentMove.getScore();
                }
            }
        }

        // Set stalemate score to 0
        if (!aBoard.isKingInCheck() && isGameEndingMove) {
            score = 0;
        }

        aBoard.undo();

        return score;
    }
}
