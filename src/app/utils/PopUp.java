package app.utils;

import app.Chessman;
import javafx.event.Event;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Luokka, jolla hallitaan paaikkunan paalle avautuvia ikkunoita
 */
public class PopUp {


    static String openWindowSelection(String title, boolean initModality, double width, final String... values) {
        Stage stage = new Stage();
        stage.setTitle(title);
        stage.setResizable(false);
        stage.sizeToScene();
        if (initModality) stage.initModality(Modality.APPLICATION_MODAL);
        VBox layout = new VBox(5);

        final String[] selectedType = new String[]{values[0]};
        for (String value : values) {
            Button button = new Button(value);
            button.setPrefWidth(width);
            button.setOnAction(event -> {
                selectedType[0] = value;
                stage.close();
            });
            button.setOnKeyPressed(event -> {
                if (event.getCode() == KeyCode.ENTER) {
                    selectedType[0] = value;
                    stage.close();
                }
            });
            layout.getChildren().add(button);
        }
        stage.setOnCloseRequest(windowEvent -> selectedType[0] = null);
        stage.setScene(new Scene(layout));
        stage.showAndWait();
        return selectedType[0];
    }
    static String openWindowSelection(String title, double width, final String... values) {
        return openWindowSelection(title, true, width, values);
    }

    /**
     * Avaa ikkunan, josta valitaan korottuuko sotilas kuningattareksi, ratsuksi, torniksi vai lahetiksi.
     *
     * @return valitun tyypin
     */
    public static Chessman.Type getPromotion() {
        String strValue = openWindowSelection("Promote to: ", true, 280, "QUEEN", "KNIGHT", "ROOK", "BISHOP");
        if (strValue == null) return null;
        return Chessman.Type.valueOf(strValue);
    }

}
