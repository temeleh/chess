package app.utils;

import com.interactivemesh.jfx.importer.ImportException;
import com.interactivemesh.jfx.importer.obj.ObjModelImporter;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.MeshView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created on 20:19 22.3.2019
 */
public class ObjLoader {

    public static List<MeshView> load(String texturePath, String objPath) {
        return ObjLoader.load(null, null, texturePath, new String[] {objPath});
    }

    public static List<MeshView> load(String textureFolder, String fileExtension, String... objPaths) {
        return ObjLoader.load(textureFolder, fileExtension, null, objPaths);
    }

    private static List<MeshView> load(String textureFolder, String fileExtension, String texturePath, String... objPaths) {
        ObjModelImporter importer = new ObjModelImporter();
        List<MeshView> meshViewList = new ArrayList<>();

        for (String objPath : objPaths) {
            try {
                importer.read(ObjLoader.class.getClassLoader().getResource(objPath));
            } catch (ImportException e) {
                e.printStackTrace();
                return null;
            }


            if (textureFolder != null && fileExtension != null) {
                Map<String, MeshView> objects = importer.getNamedMeshViews();
                List<String> names = new ArrayList<>(objects.keySet());

                for (String name : names) {
                    MeshView meshView = objects.get(name);

                    PhongMaterial material = new PhongMaterial(Color.WHITE);
                    material.setDiffuseMap(new Image(textureFolder + "/" + name + fileExtension));

                    meshView.setMaterial(material);
                    meshViewList.add(meshView);
                }

            } else {
                MeshView[] meshViews = importer.getImport();

                if (texturePath != null) {
                    for (MeshView meshView : meshViews) {
                        meshView.setMaterial(new PhongMaterial(Color.WHITE, new Image(texturePath), null, null, null));
                    }
                }

                meshViewList.addAll(Arrays.asList(meshViews));
            }
        }

        importer.close();
        return meshViewList;
    }
}
