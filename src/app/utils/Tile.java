package app.utils;

import app.Main;

/**
 * Luokka mallintaa yhta shakkilaudan ruutua. Koordinaattiparia kaytetaan myos kaksiulotteisen vektorin esittamiseen.
 * Vasemman alakulman koordinaatit ovat (0,0) ja oikean ylakulman (7,7).
 */
public class Tile {
    /**
     * x ja y koordinaatti
     */
    private int x, y;

    /**
     * Laudan rajat
     */
    private static final int MAX = Main.TILE_COUNT - 1, MIN = 0;

    /**
     * Onko Tile rajojen sisapuolella
     */
    private boolean isInBounds;

    /**
     * Luo uuden Tilen annetuilla koordinaateilla
     *
     * @param x x-koordinaatti
     * @param y y-koordinaatti
     */
    public Tile(int x, int y) {
        this.isInBounds = setXY(x, y);
    }

    public Tile() {}

    /**
     * Asettaa Tilen y- ja x-koordinaatin
     *
     * @param x x-koordinaatti
     * @param y y-koordinaatti
     * @return true, jos arvot olivat rajojen sisalla, muuten false
     */
    public boolean setXY(int x, int y) {
        boolean xInBounds = setX(x);
        boolean yInBounds = setY(y);

        isInBounds = xInBounds && yInBounds;
        return isInBounds;
    }

    /**
     * Asettaa Tilen x-koordinaatin
     *
     * @param x x-koordinaatti
     * @return true, jos arvo oli rajojen sisalla, muuten false
     */
    public boolean setX(int x) {
        this.x = x;
        this.isInBounds = x <= MAX && x >= MIN;
        return isInBounds;
    }

    /**
     * Asettaa Tilen y-koordinaatin
     *
     * @param y y-koordinaatti
     * @return true, jos arvo oli rajojen sisalla, muuten false
     */
    public boolean setY(int y) {
        this.y = y;
        this.isInBounds = y <= MAX && y >= MIN;
        return isInBounds;
    }

    /**
     * Hakee Tilen x-koordinaatin
     * @return Tilen x-koordinaatin
     */
    public int getX() {
        return x;
    }

    /**
     * Hakee Tilen y-koordinaatin
     * @return Tilen y-koordinaatin
     */
    public int getY() {
        return y;
    }

    /**
     * @return ovatko koordinaatit rajojen sisapuolella
     */
    public boolean isInBounds() {
        return this.isInBounds;
    }


    /**
     * Hakee taman tilen naapuritilen, joka eroaa tasta Tilesta tiettyjen x ja y muutosten verran
     *
     * @param xOffset muutos x suuntaan
     * @param yOffset muutos y suuntaan
     * @return uuden tilen, johon on lisatty x ja y suunnan muutokset
     */
    public Tile getNeighbor(int xOffset, int yOffset) {
        return new Tile(x + xOffset, y + yOffset);
    }
    public Tile getNeighbor(Tile dirTile) {
        return getNeighbor(dirTile.getX(), dirTile.getY());
    }


    /**
     * Palauttaa Tilen, joka vastaa tiettyyn suuntaan osoittavaa kaksiulotteista vektoria.
     *
     * Suunnat 0-3 vastaavat ilmansuuntia lansi, pohjoinen, ita, etela. (vasen, ylos, oikea, alas).
     * Suunnat 4-7 vastaavat ilmansuuntia luode, koillinen, kaakko, lounas. (Eli diagonaalisuunnat).
     * Suunnat 8-15 ovat kaikki hevosen mahdolliset siirtymat aloittaen vasemmasta ylakulmasta (-2, -1) kiertaen myotapaivaan.
     *
     *
     * @param dir suunta
     * @return uuden Tilen, joka vastaa tiettyyn suuntaan osoittavaa kaksiulotteista vektoria
     */
    public static Tile getDirTile(int dir) {
        switch (dir) {
            case 0: return new Tile(-1, 0);
            case 1: return new Tile(0, 1);
            case 2: return new Tile(1, 0);
            case 3: return new Tile(0, -1);

            case 4: return new Tile(-1, 1);
            case 5: return new Tile(1, 1);
            case 6: return new Tile(1, -1);
            case 7: return new Tile(-1, -1);

            case 8: return new Tile(-2, 1);
            case 9: return new Tile(-1, 2);
            case 10: return new Tile(1, 2);
            case 11: return new Tile(2, 1);
            case 12: return new Tile(2, -1);
            case 13: return new Tile(1, -2);
            case 14: return new Tile(-1, -2);
            case 15: return new Tile(-2, -1);

            default: return new Tile();
        }
    }



    @Override
    public String toString() {
        return x + "," + y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Tile) {
            Tile tile = (Tile) obj;

            return this.x == tile.getX() && this.y == tile.getY();
        }

        return false;
    }
}
