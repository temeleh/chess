package app.utils;

import app.Chessman;

/**
 * Kuvaa laudalla tehtavaa siirtoa
 */
public class Move {
    private Chessman chessman;
    private Tile tile;
    private Chessman.Type promoteType;

    private int score;

    public Move() {}

    public Move(Chessman chessman, Tile tile) {
        this(chessman, tile, null);
    }

    public Move(Chessman chessman, Tile tile, Chessman.Type promoteType) {
        this.chessman = chessman;
        this.tile = tile;
        this.promoteType = promoteType;
    }

    @Override
    public String toString() {
        return this.chessman.toString() + " Tile: " + tile.toString() + " Score: " + score;
    }

    public boolean isPromoting() {
        return chessman.getType() == Chessman.Type.PAWN && (tile.getY() == 0 || tile.getY() == 7);
    }



    public Chessman getChessman() {
        return chessman;
    }

    public Tile getTile() {
        return tile;
    }

    public void setPromoteType(Chessman.Type promoteType) {
        this.promoteType = promoteType;
    }

    public Chessman.Type getPromoteType() {
        return promoteType;
    }


    public void setScore(int score) {
        this.score = score;
    }

    public int getScore() {
        return score;
    }
}