package app;

import app.Chessman.Type;
import app.utils.Move;
import app.utils.Tile;
import javafx.scene.Group;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static app.Main.TILE_COUNT;

/**
 * Luokka mallintaa shakkilautaa
 */
public class Board {

    /**
     * Laudan nykyinen tila, (lista sen kaikista nappuloista talla hetkella)
     */
    protected List<Chessman> currentState = new ArrayList<>(32);

    /**
     * Laudan edelliset tilat, joihin palataan kun perutaan siirto
     */
    protected List<List<Chessman>> previousStates = new ArrayList<>();

    /**
     * Laudan alkutila, johon palataan kun aloitetaan uusi peli.
     */
    protected List<Chessman> initialState;

    /**
     * Pelattujen vuorojen maara
     */
    protected int turn;

    /**
     * Luo uuden Board olion ja lisaa lautaan nappulat, jos addChessmans == true
     *
     * @param addChessmans jos true, niin lisataan lautaan nappulat ja generoidaan niiden sallitut siirrot
     */
    public Board(boolean addChessmans) {
        if (addChessmans) {
            init();
            generateAllowedMoves();

            this.initialState = copyState(this.currentState);
        }
    }




    /**
     * Etsii nappulan, joka on tietylla tilella.
     *
     * @param tile tile
     * @return tilella olevan nappulan tai null jos tilella ei ole nappulaa
     */
    public Chessman getChessman(Tile tile) {
        for (Chessman chessman : currentState) {
            if (chessman.isAlive() && chessman.getTile().equals(tile)) {
                return chessman;
            }
        }
        return null;
    }


    /**
     * Palauttaa totuusarvon, joka kertoo kumman pelaajan vuoro on talla hetkella
     *
     * @return true jos on valkoisen vuoro, muuten false
     */
    public boolean isWhiteTurn() {
        return turn % 2 == 0;
    }


    /**
     * Tyhjentaa edelliset tilat listan
     */
    public void clearPreviousStates() {
        this.previousStates.clear();
    }

    /**
     * Poistaa viimeisimman edellisen siirron tilan
     */
    public void removePreviousState() {
        if (!this.previousStates.isEmpty()) {
            this.previousStates.remove(previousStates.size() - 1);
        }
    }


    //******************************************************************//
    // Metodit laudan tilan tallentamiseen, lataamiseen ja alustamiseen //
    //******************************************************************//

    /**
     * Luo kopion Chessman listasta, mutta ei generoi uutta 3d meshView dataa Chessmaneille
     *
     * @param state kopioitava tila
     * @return kopion parametrina saadusta tilasta ilman mesh dataa
     */
    protected List<Chessman> copyState(List<Chessman> state) {
        List<Chessman> copy = new ArrayList<>(32);
        for (Chessman chessman:state) {
            copy.add(chessman.getNoMeshChessman());
        }
        return copy;
    }

    /**
     * Hakee laudan nykyisen tilan
     *
     * @return nykyinen tila
     */
    public List<Chessman> getState() {
        return this.currentState;
    }

    /**
     * Asettaa laudan nykyisen tilan.
     *
     * Kaydaan lapi jokainen nykyisen tilan nappula ja
     * siirretaan ne uuden tilan antamille paikoille, seka asetetaan muutkin attribuutit samoiksi.
     *
     * @param state Chessman lista
     */
    protected void setState(List<Chessman> state) {
        for (int i = 0; i < this.currentState.size(); i++) {
            Chessman current = this.currentState.get(i);
            Chessman chessman = state.get(i);

            if (current.getType() != chessman.getType()) {
                current.setType(chessman.getType());

                if (!(this instanceof AnalysisBoard)) {
                    current.resetMeshView();
                }
            }

            current.moveTo(chessman.getTile());

            current.setEnPassantEatable(chessman.isEnPassantEatable());
            current.setHasMoved(chessman.hasMoved());
            current.setAlive(chessman.isAlive());
            current.setAllowedTiles(new ArrayList<>(chessman.getAllowedTiles()));

            if (current.getMeshView() != null) {
                if (!current.isAlive()) {
                    current.moveToBoardSide();
                }
            }
        }
    }

    /**
     * Alustaa laudan tilan, eli luo nappulat ja niiden mesh datan,
     * seka lisaa ne currentState listaan ensimmaisen kerran.
     */
    private void init() {
        Type[] rkbkq = new Type[]{Type.ROOK, Type.KNIGHT, Type.BISHOP, Type.QUEEN, Type.KING, Type.BISHOP, Type.KNIGHT, Type.ROOK};

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < TILE_COUNT * 2; j++) {
                int x = j < TILE_COUNT ? j : j - TILE_COUNT;
                int y = i == 0 ? (j < TILE_COUNT ? 0 : 1) : (j < TILE_COUNT ? 7 : 6);

                Chessman chessman = new Chessman(x, y, j < TILE_COUNT ? rkbkq[j] : Type.PAWN, i == 0, true);

                chessman.initBoardSideLocation(j);

                currentState.add(chessman);
            }
        }
    }

    /**
     * Palauttaa laudan alkutilaan
     */
    public void reset() {
        turn = 0;
        setState(initialState);
        clearPreviousStates();
    }

    /**
     * Hakee nykyisen laudan tilan merkkijonona muodossa: Chessman:Chessman:Chessman:...:turn
     * Jokaisen nappulan tiedot tulevat muodossa: x,y,type,isWhite,isAlive,hasMoved,enPassantEatable
     *
     * @return merkkijonon nykyisesta laudan tilasta
     */
    public String save() {
        StringBuilder value = new StringBuilder(currentState.get(0).toString());
        for(int i = 1; i < currentState.size(); i++) {
            value.append(":").append(currentState.get(i).toString());
        }
        return value + ":" + turn;
    }


    /**
     * Asettaa nykyisen laudan tilan merkkijonona saadusta tilasta
     *
     * @param s muotoa Chessman:Chessman:Chessman:...:turn oleva merkkijono
     */
    public void load(String s) {
        String[] strings = s.split(":");
        turn = Integer.parseInt(strings[strings.length-1]);
        String[] s1;
        ArrayList<Chessman> loadedState = new ArrayList<>(32);
        for(int i = 0; i < strings.length-1; i++) {
            s1 = strings[i].split(",");
            Chessman chessman = new Chessman(Integer.parseInt(s1[0]), Integer.parseInt(s1[1]), Chessman.Type.valueOf(s1[2]), Boolean.parseBoolean(s1[3]), false);
            if(s1[4].equals("false")) {
                chessman.setAlive(false);
            }
            if(s1[5].equals("true")) {
                chessman.setHasMoved(true);
            }
            if(s1[6].equals("true")) {
                chessman.setEnPassantEatable(true);
            }
            loadedState.add(chessman);
        }

        setState(loadedState);
        generateAllowedMoves();
        clearPreviousStates();
    }

    /**
     * Palauttaa laudan edelliseen tilaan, jos edellinen tila on tallennettuna
     */
    public void undo() {
        if (!previousStates.isEmpty()) {
            turn -= 1;
            setState(previousStates.get(previousStates.size() - 1));
            removePreviousState();
        }
    }


    //******************************//
    // Metodit siirtojen hallintaan //
    //******************************//


    /**
     * Hakee annetusta suunnasta hyokkaavan nappulan seka
     * kaikki Tilet, jotka kaydaan lapi tahan suuntaan mentaessa.
     *
     * Suunta muutetaan x ja y muutoksiksi Tile luokan metodin getDirTile mukaan.
     *
     * Palautuva nappula on null, jos tullaan laudan rajalle
     * tai jos doAttackChecks == true ja vastaan tullut nappula ei hyokkaa takaisin.
     *
     * Palautuvat Tilet lista ei sisalla vastaantullutta nappulaa, jos se on oma nappula. (Silla omaa nappulaa ei voi syoda).
     * Joten palautuvat Tilet ovat aina sallitut siirrot tiettyyn suuntaan mentaessa.
     *
     * @param dir suunta
     * @param from Tile, jolta tarkistus aloitetaan
     * @param doAttackChecks jos true, niin tehdaan tarkistukset, jotka varmistavat etta palautuva nappula
     *                       on aina vastapuolen nappula ja etta se hyokkaa takaisin Tilelle, jolta tarkistus aloitettiin.
     * @param onlyFirst jos true, niin kaydaan lapi vain ensimmainen askel annettuun suuntaan.
     * @return javafx.util.Pair avain/arvo parin, jossa on avaimena suunnasta hyokkaava nappula ja arvona lapikaydyt Tilet listana.
     */
    public Pair<Chessman, List<Tile>> directionCheck(int dir, Tile from, boolean doAttackChecks, boolean onlyFirst) {
        boolean first = true; // ensimmainen iteraatio
        Tile dirTile = Tile.getDirTile(dir); // muunnetaan suunta x ja y muutoksiksi
        Tile t = from.getNeighbor(dirTile);
        Chessman chessman = getChessman(t);

        List<Tile> traversedTiles = new ArrayList<>();

        // Jos tarkastetaan hevosen tai kuninkaan siirtoa, niin kaydaan lapi vain ensimmainen iteraatio.
        // Muuten jatketaan iteraatioita, kunnes loydetaan chessman tai tullaan laudan rajalle.
        if (!onlyFirst) {
            while (t.isInBounds() && chessman == null) {
                // lisataan kaytyjen tilejen listaan se tile jota talla hetkella tarkastetaan
                traversedTiles.add(t);

                t = t.getNeighbor(dirTile);
                chessman = getChessman(t);
                first = false;
            }
        }

        if (chessman == null) { // jos ei loytynyt chessmania
            // lisataan viimeisin tile kaytyjen tilejen listaan, jos se on laudalla.
            if (t.isInBounds()) traversedTiles.add(t);

        } else {
            if (chessman.isWhite() != isWhiteTurn() && chessman.isAlive()) {
                // loydettiin vastustajan chessman, joten lisataan se kaytyjen Tilejen listaan
                traversedTiles.add(t);

                // hyokkaystarkistukset asettavat chessman = null, jos tama vastustajan chessman ei hyokkaa
                // takaisin tarkistuksen aloittaneeseen tileen.
                if (doAttackChecks) {
                    if (first) { // ensimmainen iteraatio
                        // sotilas hyokkaa vain eteenpain oleville diagonaaleille
                        if (chessman.getType() == Type.PAWN && !(isWhiteTurn() ? dir == 4 || dir == 5 : dir == 6 || dir == 7)
                                || chessman.getType() == Type.KNIGHT && dir < 8) {
                            chessman = null;
                        }
                    } else { // joku muu iteraatio
                        // jos kuningas, hevonen tai sotilas loydetaan yli yhden tilen/iteraation paasta, niin ei palauteta niita, silla
                        // ne voivat hyokata vain yhden tilen/iteraation paahan.
                        if (chessman.getType() == Type.KING || chessman.getType() == Type.PAWN || chessman.getType() == Type.KNIGHT) {
                            chessman = null;
                        }
                    }

                    // tehdaan viela tarkistukset, jotka asettavat chessman = null, jos se loydetaan suunnasta, josta se ei hyokkaa takaisin.
                    if (chessman != null && (
                            chessman.getType() != Type.KNIGHT && dir >= 8 ||
                            chessman.getType() == Type.BISHOP && dir < 4 ||
                            chessman.getType() == Type.ROOK && dir > 3)) {
                        chessman = null;
                    }
                }
                return new Pair<>(chessman, traversedTiles);
            }
        }
        return new Pair<>(null, traversedTiles);
    }

    /**
     * Hakee annetusta suunnasta hyokkaavan nappulan
     *
     * @param dir suunta
     * @param from Tile, johon hyokkaavaa nappulaa haetaan
     * @return Tilelle hyokkaavan nappulan tai null, jos suunnasta ei hyokkaa mikaan nappula
     */
    public Chessman attackFromDir(int dir, Tile from) {
        return directionCheck(dir, from, true, false).getKey();
    }

    /**
     * Hakee annetusta suunnasta kaikki Tilet, jotka kaydaan lapi tahan suuntaan mentaessa.
     *
     * @param dir suunta
     * @param from Tile, jolta tarkistus aloitetaan
     * @param onlyFirst jos true, niin kaydaan lapi vain ensimmainen askel annettuun suuntaan.
     * @return listan lapikaydyista Tileista
     */
    public List<Tile> tilesFromDir(int dir, Chessman from, boolean onlyFirst) {
        return directionCheck(dir, from.getTile(), false, onlyFirst).getValue();
    }

    /**
     * Tekee siirron, eli siirtaa siirrettavan nappulan annetulle tilelle.
     * Metodi myos hallitsee vastustajan nappulan syomisen ja tornin siirtymisen linnoittautuessa.
     * Metodi ei tarkista onko siirto sallittu, vaan olettaa, etta siirto on tarkistettu ennen taman metodin kayttoa.
     * Jos promotedType ei ole null, niin metodi korottaa annetun Chessmanin tyypin annetuksi tyypiksi.
     *
     * @param chessman siirrettava nappula
     * @param target Tile, jonne nappulaa siirretaan
     * @param promotedType Type, joksi siirrettavan nappulan tyyppi muutetaan
     * @param generateNextMoves jos true, niin generoidaan nappuloille sallitut siirrot siirron jalkeen
     *                          ja kasvatetaan vuoronumeroa
     */
    public void makeMove(Chessman chessman, Tile target, Type promotedType, boolean generateNextMoves) {
        previousStates.add(copyState(currentState));

        Chessman capturedChessman = getChessman(target);

        // syodaan sotilas ohestalyonnilla, jos tama siirto oli ohestalyonti
        int deltaX = chessman.getTile().getX() - target.getX();
        if (chessman.getType() == Type.PAWN && capturedChessman == null && Math.abs(deltaX) == 1) {
            capturedChessman = getChessman(target.getNeighbor(0, isWhiteTurn() ? -1 : 1));
        }

        // siirretaan torni kuninkaan vierelle linnottautuessa
        if (chessman.getType() == Type.KING && Math.abs(deltaX) == 2) {
            Chessman c = getChessman(new Tile(deltaX > 0 ? 0 : 7, chessman.isWhite() ? 0 : 7));
            c.moveTo(chessman.getNeighborTile(deltaX > 0 ? -1 : 1, 0));
        }

        if (capturedChessman != null) {
            capturedChessman.setAlive(false);
            capturedChessman.moveToBoardSide();
        }

        // asetetaan tämä siirrettava sotilas syotavaksi ohestalyonnilla, jos sita siirretaan 2 askelta eteenpain (ensimmainen siirto).
        if (chessman.getType() == Type.PAWN && Math.abs(target.getY() - chessman.getTile().getY()) == 2) {
            chessman.setEnPassantEatable(true);
        }

        if (promotedType != null) {
            chessman.promote(promotedType);
        }

        chessman.moveTo(target);

        if (generateNextMoves) {
            turn++;
            generateAllowedMoves();
        }
    }

    public void makeMove(Move move, boolean generateNextMoves) {
        this.makeMove(move.getChessman(), move.getTile(), move.getPromoteType(), generateNextMoves);
    }

    /**
     * Generoi kaikille laudalla oleville nappuloille sallitut siirrot ottaen huomioon kumman pelaajan vuoro on,
     * eli jos on valkoisen vuoro, niin generoidaan vain valkoisten nappuloiden siirrot ja asetetaan kaikkien mustien
     * nappuloiden sallittujen siirtojen listat tyhjiksi.
     *
     * Ensin luodaan Tile lista kaikista siirroista, johon nappula voi siirtya ja sitten poistetaan tasta listasta
     * ne siirrot, jotka laittavat oman (eli sen kumman vuoro on) kuninkaan shakkiin.
     */
    private void generateAllowedMoves() {
        AnalysisBoard aBoard = new AnalysisBoard(this);

        for (Chessman chessman : currentState) {
            if (!chessman.isAlive()) continue;

            //System.out.print(isWhiteTurn() != chessman.isWhite());

            if (isWhiteTurn() != chessman.isWhite()) {
                chessman.setAllowedTiles(Collections.emptyList());
            } else {

                List<Tile> allowed = new ArrayList<>();

                switch (chessman.getType()) {
                    case PAWN:
                        for (int i = 1; i <= 5; i++) {
                            // valkoiselle kaydaan lapi suunnat i, eli 1 - 5, joista skipataan 2 ja 3 (oikea ja alas)
                            // mustalle kaydaan lapi suunnat i + 2, silla eri pelaajien suunnat ovat 2 suunnan paassa toisistaan!

                            // Tasta seuraa se, etta molemmille pelaajille kaydaan lapi vastaavat suunnat:
                            // eteenpain ja molemmat eteenpain menevät diagonaalit. (Mustalle eteenpain on alas!)
                            Tile dir = Tile.getDirTile(isWhiteTurn() ? i : i + 2);

                            Tile neigbrTile = chessman.getNeighborTile(dir);
                            Chessman neighbor = getChessman(neigbrTile);

                            // jos naapuri on oma chessman, niin ei lisata sen tilea sallituksi siirroksi,
                            // tai jos tarkistetaan suoraan edessa olevaa tilea (i == 1), niin lisataan tile sallituksi
                            // siirroksi jos tile on tyhja. Jos chessman on sen sijaan hyokkaamassa vasemmalle tai oikealle,
                            // niin lisataan sallittu siirto vain, jos tilella on chessman.
                            if (neighbor != null && neighbor.isWhite() == isWhiteTurn() || ((i == 1) == (neighbor != null))) {
                                neigbrTile = null;
                            }

                            // nyt neigbrTile (jos olemassa) on sallittu suunta sotilaalle, joten lisataan se listaan.
                            if (neigbrTile != null) allowed.add(neigbrTile);

                            // skipataan suunnat 2 ja 3,
                            // tarkistetaan saako sotilas siirtya kaksi siirtoa eteenpain (ensimmainen siirto)
                            if (i == 1) { // i == 1, eli suunta eteenpain
                                i += 2;

                                // Jos chessman ei ole liikkunut pelin aikana, ja edessa olevalle tilelle on paasy,
                                // niin tarkistetaan paastaanko kahden tilen paahan samalla tavalla, kuin tarkistettiin edessa olevalle tilelle paasy.
                                if (!chessman.hasMoved() && neigbrTile != null) {
                                    Tile forward2Tile = chessman.getNeighborTile(dir).getNeighbor(dir);
                                    Chessman forward2Neighbor = getChessman(forward2Tile);

                                    if (forward2Neighbor != null && forward2Neighbor.isWhite() == isWhiteTurn() || forward2Neighbor != null) {
                                        forward2Tile = null;
                                    }
                                    if (forward2Tile != null) allowed.add(forward2Tile);
                                }
                            }
                        }


                        // Tarkistetaan voiko sotilas syoda ohestalyonnilla
                        // ja lisataan oikea tile sallittujen siirtojen listaan jos voi.
                        for (int i = 0; i < 2; i++) {
                            Chessman rightAndLeft = getChessman(chessman.getTile().getNeighbor(Tile.getDirTile(i * 2)));

                            if (rightAndLeft != null && rightAndLeft.isEnPassantEatable() && rightAndLeft.isWhite() != isWhiteTurn()) {
                                allowed.add(rightAndLeft.getTile().getNeighbor(Tile.getDirTile(isWhiteTurn() ? 1 : 3)));
                            }
                        }

                        // sotilas on syotavissa ohestalyonnilla vain yhden siirron ajan, joten seuraavalla
                        // siirtojen tarkistuskerralla poistetaan tama ohestalyontimahdollisuus.
                        chessman.setEnPassantEatable(false);

                        break;
                    case BISHOP:
                        for (int i = 4; i <= 7; i++) allowed.addAll(tilesFromDir(i, chessman, false));
                        break;
                    case KNIGHT:
                        for (int i = 8; i <= 15; i++) allowed.addAll(tilesFromDir(i, chessman, true));
                        break;
                    case ROOK:
                        for (int i = 0; i <= 3; i++) allowed.addAll(tilesFromDir(i, chessman, false));
                        break;
                    case QUEEN:
                        for (int i = 0; i <= 7; i++) allowed.addAll(tilesFromDir(i, chessman, false));
                        break;
                    case KING:
                        for (int i = 0; i <= 7; i++) allowed.addAll(tilesFromDir(i, chessman, true));

                        // lisataan linnoittautumisen siirrot, jos linnottautuminen on mahdollista
                        if (!chessman.hasMoved() && !isTileAttacked(chessman.getTile())) {
                            for (int e = 4; e >= 3; e--) {
                                Tile rookTile = chessman.getNeighborTile(e == 4 ? -e : e, 0);
                                Chessman rook = getChessman(rookTile);

                                if (rook != null && !rook.hasMoved()) {
                                    boolean clear = true;
                                    for (int i = 1; i < e; i++) {
                                        Tile neigbrTile = chessman.getNeighborTile(e == 4 ? -i : i, 0);
                                        Chessman neighbor = getChessman(neigbrTile);
                                        if (neighbor != null || (isTileAttacked(neigbrTile) && i != 3)) {
                                            clear = false;
                                        }
                                    }
                                    if (clear) {
                                        allowed.add(new Tile(e == 4 ? 2 : 6, chessman.getTile().getY()));
                                    }
                                }
                            }

                        }
                }

                // oma kuningas ei saa tulla shakkiin siirron jalkeen, joten kaydaan lapi jokainen siirto ja poistetaan
                // ne allowed listasta, jos kuningas tulee shakkiin siirron jalkeen.
                for (int i = allowed.size() - 1; i >= 0; i--) {
                    aBoard.makeAnalysisMove(chessman, allowed.get(i));
                    if (aBoard.isKingInCheck()) {
                        allowed.remove(i);
                    }
                    aBoard.reset();
                }

                chessman.setAllowedTiles(allowed);
            }

        }
    }

    /**
     * Tarkistaa tuleeko annetulle tilelle hyokkayksia
     *
     * @param tile tarkistettava tile
     * @return true, jos vastustajan nappula hyokkaa tilelle jostain suunnasta, muuten false
     */
    public boolean isTileAttacked(Tile tile) {
        for (int i = 0; i < 16; i++) { // 16 suuntaa
            if (attackFromDir(i, tile) != null) {
                return true;
            }
        }
        return false;
    }

    /**
     * Hakee sen kuninkaan, jonka vuoro on
     *
     * @return oman kuninkaan
     */
    public Chessman getOwnKing() {
        return this.currentState.get(isWhiteTurn() ? 4 : 20);
    }


    /**
     * Hakee laudan nappuloiden mesh datat
     *
     * @return ryhman, jossa on jokaisen laudan nappulan MeshView
     */
    public Group getChessmanMeshViews() {
        Group g = new Group();
        for (Chessman chessman:currentState) {
            g.getChildren().add(chessman.getMeshView());
        }
        return g;
    }

}







































