package app;

import app.utils.ObjLoader;
import app.utils.Tile;
import javafx.geometry.Point3D;
import javafx.scene.shape.MeshView;
import javafx.scene.transform.Rotate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static app.Main.*;

/**
 * Luokka mallintaa shakkilaudan yhta nappulaa
 */
public class Chessman {

    public enum Type {
        PAWN(1), BISHOP(3), KNIGHT(3), ROOK(5), QUEEN(9), KING(100000);

        private final int value;
        Type(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }


    private Tile location;
    public Tile getTile() {
        return location;
    }


    private List<Tile> allowedTiles = Collections.emptyList();
    public List<Tile> getAllowedTiles() {
        return allowedTiles;
    }
    public void setAllowedTiles(List<Tile> allowedTiles) {
        this.allowedTiles = allowedTiles;
    }



    private Type type;
    public Type getType() { return type;}
    public void setType(Type type) {this.type = type;}


    private boolean isWhite;
    public boolean isWhite() {
        return isWhite;
    }



    private boolean isAlive;
    public boolean isAlive() {
        return isAlive;
    }
    public void setAlive(boolean alive) {
        isAlive = alive;
    }


    private boolean hasMoved;
    public boolean hasMoved() {
        return hasMoved;
    }
    public void setHasMoved(boolean hasMoved) {
        this.hasMoved = hasMoved;
    }



    private boolean enPassantEatable;
    public boolean isEnPassantEatable() {
        return enPassantEatable;
    }
    public void setEnPassantEatable(boolean enPassantEatable) {
        this.enPassantEatable = enPassantEatable;
    }



    private MeshView meshView;
    public MeshView getMeshView() {
        return meshView;
    }


    private Point3D boardSideLocation;
    public void initBoardSideLocation(int chessmanId) {
        double x, z;

        if (chessmanId < 5) {
            x = SIDE_XRIGHT + (4 - chessmanId) * 0.5;
            z = SIDE_ZBASE - (SIDE_ZOFFSET * 2) * chessmanId;

        } else if (chessmanId < 8) {
            chessmanId = 2 - (chessmanId - 5);
            x = SIDE_XLEFT + (4 - chessmanId) * 0.5;
            z = SIDE_ZBASE - SIDE_ZOFFSET - (SIDE_ZOFFSET * 2) * chessmanId;
        } else {
            chessmanId -= 8;
            x = chessmanId % 2 != 0 ? SIDE_XRIGHT : SIDE_XLEFT;
            z = SIDE_ZPAWNBASE - SIDE_ZOFFSET * chessmanId;
        }

        z = this.isWhite ? z : -z;


        this.boardSideLocation = new Point3D(x + XZ_CENTER_CORRECTION, TABLE_YLEVEL, z + XZ_CENTER_CORRECTION);
    }

    public void moveToBoardSide() {
        if (meshView != null && boardSideLocation != null) {
            meshView.setTranslateX(boardSideLocation.getX());
            meshView.setTranslateZ(boardSideLocation.getZ());
            meshView.setTranslateY(boardSideLocation.getY());
        }
    }



    /**
     * Luo uuden Chessman olion annetuilla attribuuteilla.
     *
     * @param x x-koordinaatti
     * @param y y-koordinaatti
     * @param type tyyppi
     * @param isWhite true, jos valkoinen, muuten false
     * @param generateMeshView jos true, niin generoidaan nappulalle 3d mesh data
     */
    public Chessman(int x, int y, Type type, boolean isWhite, boolean generateMeshView) {
        this.location = new Tile(x, y);
        this.type = type;
        this.isWhite = isWhite;
        this.isAlive = true;

        if (generateMeshView) this.meshView = generateMeshView(type);
    }
    public Chessman(Tile tile, Type type, boolean isWhite, boolean generateMeshView) {
        this(tile.getX(), tile.getY(), type, isWhite, generateMeshView);
    }


    /**
     * Luo uuden Chessman olion taman Chessmanin attribuuteilla, joka ei sisalla 3d mesh dataa
     *
     * @return uuden Chessman olion ilman mesh dataa
     */
    public Chessman getNoMeshChessman() {
        Chessman c = new Chessman(this.location, this.type, this.isWhite, false);
        c.isAlive = this.isAlive;
        c.hasMoved = this.hasMoved;
        c.enPassantEatable = this.enPassantEatable;
        c.allowedTiles = new ArrayList<>(this.allowedTiles);

        return c;
    }


    /**
     * Hakee taman nappulan tilen naapuritilen, joka eroaa tasta Tilesta tiettyjen x ja y muutosten verran
     *
     * @param xOffset muutos x suuntaan
     * @param yOffset muutos y suuntaan
     * @return uuden tilen, johon on lisatty x ja y suunnan muutokset
     */
    public Tile getNeighborTile(int xOffset, int yOffset) {
        return location.getNeighbor(xOffset, yOffset);
    }
    public Tile getNeighborTile(Tile dir) {
        return location.getNeighbor(dir);
    }


    /**
     * Hakee merkkijonon taman nappulan attribuuteista muodossa:
     * x,y,type,isWhite,isAlive,hasMoved,enPassantEatable
     *
     * @return merkkijonon nappulan attribuuteista
     */
    @Override
    public String toString() {
        return location.toString() + "," + type + "," + isWhite + "," + isAlive + "," + hasMoved + "," + enPassantEatable;
    }


    /**
     * Siirtaa nappulan parametrina saadulle Tilelle
     *
     * @param tile tile, jolle nappula siirretaan
     */
    public void moveTo(Tile tile) {
        hasMoved = true;
        location.setXY(tile.getX(), tile.getY());

        if (this.meshView != null) {
            this.meshView.setTranslateX(tile.getX() * TILE_LENGTH);
            this.meshView.setTranslateZ(tile.getY() * TILE_LENGTH);
            this.meshView.setTranslateY(0);
        }
    }

    /**
     * Korottaa nappulan annetuksi tyypiksi
     *
     * @param type korotettu tyyppi
     */
    public void promote(Type type) {
        this.type = type;
        if (meshView != null) resetMeshView();
    }


    /**
     * Luo nappulalle uuden MeshView:n nappulan tyypin mukaan.
     */
    public void resetMeshView() {
        MeshView meshView = generateMeshView(this.type);

        this.meshView.setMesh(meshView.getMesh());
        this.meshView.setMaterial(meshView.getMaterial());
        this.meshView.setRotate(meshView.getRotate());
    }

    /**
     * Hakee uuden MeshView:n parametrina saadusta tyypista, taman nappulan sijaintiin.
     *
     * @param type generoitava tyyppi
     * @return uusi MeshView
     */
    private MeshView generateMeshView(Type type) {
        MeshView meshView = ObjLoader.load(
                "textures/" + (isWhite ? "white" : "black") + "/" + type.toString() + ".jpg",
                "objects/" + type.toString() + ".obj").get(0);

        meshView.setMouseTransparent(true);
        meshView.setTranslateX(this.location.getX() * TILE_LENGTH);
        meshView.setTranslateZ(this.location.getY() * TILE_LENGTH);

        meshView.setRotationAxis(Rotate.Y_AXIS);

        if (!this.isWhite && this.type.equals(Type.KNIGHT)) {
            meshView.setRotate(180);
        }
        return meshView;
    }
}
