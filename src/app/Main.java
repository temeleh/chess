package app;

import app.ai.SimpleEngine;
import app.utils.Move;
import app.utils.ObjLoader;
import app.utils.PopUp;
import app.utils.Tile;
import javafx.animation.RotateTransition;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point3D;
import javafx.scene.*;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.MeshView;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

/**
 * Luokka hallitsee kaiken graafisen liittyman sisallon.
 */
public class Main implements Initializable {

    // root elementti
    @FXML
    private BorderPane root;

    // ryhma, joka sisaltaa kaikki elementit, joita kierretaan, kun lautaa kierretaan
    private Group group3d;

    // asetukset
    @FXML
    private CheckMenuItem rotateAfterMove;
    @FXML
    private CheckMenuItem allowUndo;

    private RotateTransition boardRotation;


    public static final double TILE_LENGTH = 6.0048;
    public static final int TILE_COUNT = 8;

    public static final int TILES_INDEX = 6;

    public static final int TIMER_INDEX = 5;
    public static final double TIMER_ROTATE_ANGLE = 9;


    public static final double XZ_CENTER_CORRECTION = 7 * (TILE_LENGTH / 2);

    public static final double TABLE_YLEVEL = 1.2;

    public static final double SIDE_XRIGHT = -45;
    public static final double SIDE_XLEFT = SIDE_XRIGHT - 4;

    public static final double SIDE_ZOFFSET = 2.2;
    public static final double SIDE_ZBASE = 34.4;
    public static final double SIDE_ZPAWNBASE = SIDE_ZBASE - 7 * SIDE_ZOFFSET;



    /**
     * Tile, johon hiiri osoittaa
     */
    private Tile hover = new Tile();

    /**
     * Tile, jota viimeiseksi ollaan klikattu
     */
    private Tile press = new Tile();


    // highlight-kuvat
    private Image selectedImg = new Image("/textures/tiles/tileSelect.png");
    private Image allowedImg = new Image("/textures/tiles/tileAllowed.png");
    private Image allowedEatImg = new Image("/textures/tiles/tileAllowedEat.png");
    private Image checkedImg = new Image("/textures/tiles/tileChecked.png");
    private List<List<ImageView>> highlightImgs;
    private ImageView hoverImg = getImage3d("/textures/tiles/tileHover.png", 0.05);


    private Chessman selectedChessman;

    private Board board;

    private boolean onDrag;

    private MeshView timerButton;


    // Tekoälyn hallinta

    private boolean ai;

    private SimpleEngine simpleAi;

    private Service<Move> aiService = new Service<Move>() {
        @Override
        protected Task<Move> createTask() {
            return new Task<Move>() {
                @Override
                protected Move call() {
                    try {
                        return simpleAi.getBestMove(5);
                    } catch (Exception e) {
                        return null;
                    }
                }
            };
        }
    };


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Camera camera = initCamera();

        List<MeshView> meshViews = ObjLoader.load("textures", ".jpg", "objects/Scene.obj");

        Group staticGroup = new Group(meshViews.toArray(new MeshView[0]));

        timerButton = meshViews.get(TIMER_INDEX);
        timerButton.setRotationAxis(Rotate.X_AXIS);

        board = new Board(true);
        group3d = new Group(initLights(), staticGroup, board.getChessmanMeshViews());

        initMouseListeners(meshViews.get(TILES_INDEX));
        initHighlightImgs();

        SubScene sb = new SubScene(group3d, root.getPrefWidth(), root.getPrefHeight(), true, SceneAntialiasing.BALANCED);
        sb.setFill(Color.BLACK);
        sb.setCamera(camera);
        sb.widthProperty().bind(root.widthProperty());
        sb.heightProperty().bind(root.heightProperty());

        root.setCenter(sb);

        boardRotation = new RotateTransition(Duration.seconds(2.5), group3d);
        boardRotation.setAxis(Rotate.Y_AXIS);
        boardRotation.setByAngle(-180);

        simpleAi = new SimpleEngine(board);

        aiService.setOnSucceeded(event -> {
            Move aiMove = aiService.getValue();

            if (aiMove != null) {
                board.makeMove(aiMove, true);
            }

            setSelected(null);
        });
    }


    /**
     * Asettaa hiiren kuuntelijat, jotka hoitavat hiiren interaktiot laudan kanssa,
     * kuten tilejen valitsemisen, kun niita klikataan hiirella.
     */
    private void initMouseListeners(MeshView tiles) {
        tiles.setOnMouseMoved(this::setHover);
        tiles.setOnMouseExited(event -> setHover(null));

        tiles.setOnMouseDragged(event -> {
            if (aiService.isRunning()) return;

            if (event.getButton() == MouseButton.PRIMARY && hover.isInBounds()) {
                setHover(event);
                setSelected(board.getChessman(press));
                onDrag = true;
            }
        });

        tiles.setOnMousePressed(event -> {
            timerButton.setRotate(TIMER_ROTATE_ANGLE);
            if (aiService.isRunning()) return;

            if (event.getButton() != MouseButton.PRIMARY || !hover.isInBounds()) {
                return;
            }

            press.setXY(hover.getX(), hover.getY());

            Chessman clicked = board.getChessman(press);

            if (selectedChessman != null && (clicked == null || clicked.isWhite() != board.isWhiteTurn())) {
                // tehdaan siirto
                makeMove(selectedChessman, press);
            } else if (clicked != null && !clicked.equals(selectedChessman)) {
                // valitaan pala, kun klikattiin toista chessmania
                setSelected(clicked);
            } else if (!onDrag) {
                // poistetaan valinta, kun klikattiin samaa chessmania uudelleen, eika olla siirtamassa palaa
                setSelected(null);
            }

        });
        tiles.setOnMouseReleased(event -> {
            if (aiService.isRunning()) return;

            if (event.getButton() == MouseButton.PRIMARY && hover.isInBounds()) {

                if (!press.equals(hover)) { // siirretaan palaa yhdella klikkauksella, (raahaus)
                    Chessman clicked = board.getChessman(press);

                    if (clicked != null) { // aloitettiin raahaus chessmanin kohdalta
                        makeMove(clicked, hover);
                    }
                }
            } else {
                setSelected(null);
            }
            onDrag = false;
        });
    }

    /**
     * Alustaa highlight-kuvat ja lisaa ne highlightImgs listaan
     */
    private void initHighlightImgs() {
        highlightImgs = new ArrayList<>(8);

        for (int i = 0; i < 8; i++) {
            List<ImageView> subList = new ArrayList<>(8);
            for (int j = 0; j < 8; j++) {
                ImageView highlight = getImage3d(null);
                setImageXZ(highlight, i, j);
                highlight.setVisible(false);
                subList.add(highlight);
            }
            highlightImgs.add(subList);
        }

        Group boardM = (Group) group3d.getChildren().get(1);
        boardM.getChildren().add(hoverImg);
        for (List<ImageView> list : highlightImgs) boardM.getChildren().addAll(list);
    }

    /**
     * Tekee siirron, eli siirtaa siirrettavan nappulan annetulle tilelle, mutta vain jos siirto on sallittu.
     * Metodi myos kysyy kayttajalta kehitettavan tyypin, kun sotilas paasee laudan rajalle.
     *
     * @param chessman siirrettava Chessman
     * @param target   tile, jolle nappulaa siirretaan
     */
    private void makeMove(Chessman chessman, Tile target) {
        // Ei sallittu siirto
        if (!chessman.getAllowedTiles().contains(target)) {
            return;
        }

        Move move = new Move(chessman, target);

        if (move.isPromoting()) {
            Chessman.Type promoteType = PopUp.getPromotion();
            if (promoteType == null) return;

            move.setPromoteType(promoteType);
        }

        board.makeMove(move, true);

        setSelected(null);
        if (!ai && rotateAfterMove.isSelected()) rotateBoard();

        if (ai) makeAiMove();
    }


    private void makeAiMove() {
        aiService.reset();
        aiService.start();
    }


    /**
     * Asettaa valitsimen tilen paalle, jos hiirella osoitetaan jotain tilea.
     * Jos event == null, niin piilotetaan valitsimen kuva.
     *
     * @param event MouseEvent, josta saadaan hiiren sijainti
     */
    private void setHover(MouseEvent event) {
        if (event != null) {
            Point3D point = event.getPickResult().getIntersectedPoint();
            int x = (int) Math.floor((4 * TILE_LENGTH + point.getX()) / TILE_LENGTH);
            int z = (int) Math.floor((4 * TILE_LENGTH + point.getZ()) / TILE_LENGTH);

            hover.setXY(x, z);
            if (hover.isInBounds()) {
                setImageXZ(hoverImg, x, z);
                return;
            }
        }

        hoverImg.setVisible(false);
    }

    /**
     * Asettaa parametrina saadun nappulan valituksi seka metodilla setHighlights taman nappulan sallitut siirrot
     * Jos chessman == null, niin asettuu selectedChessman = null ja setHighlights(null).
     *
     * @param chessman nappula, joka asetetaan valituksi
     */
    private void setSelected(Chessman chessman) {
        selectedChessman = chessman;

        if (chessman != null) {
            if (chessman.isWhite() == board.isWhiteTurn()) {
                setHighlights(chessman);
            }
        } else {
            setHighlights(null);
        }
    }

    /**
     * Asettaa highlight-kuvat nappulan mukaan, eli talta nappulalta naytetaan sallitut siirrot.
     * Jos chessman == null, niin piilotetaan kaikki paitsi shakissa olevan kuninkaan alla olevan tilen highlight.
     *
     * @param chessman Chessman, jonka sallitut siirrot naytetaan
     */
    private void setHighlights(Chessman chessman) {
        for (List<ImageView> subList : highlightImgs) {
            for (ImageView img : subList) {
                img.setVisible(false);
            }
        }
        if (chessman != null) {
            List<Tile> tiles = chessman.getAllowedTiles();
            for (Tile tile : tiles) {
                showHighlightImg(tile, board.getChessman(tile) == null ? allowedImg : allowedEatImg);
            }
            showHighlightImg(chessman.getTile(), selectedImg);
        }

        Chessman king = board.getOwnKing();
        if (board.isTileAttacked(king.getTile())) showHighlightImg(king.getTile(), checkedImg);
    }

    /**
     * Asettaa tietylla tilella olevan highlight-kuvan nakyvaksi
     *
     * @param tile  tile, jonka koordinaateissa oleva highlight-kuva asetetaan nakyvaksi
     * @param image naytettava kuva
     */
    private void showHighlightImg(Tile tile, Image image) {
        ImageView highlight = highlightImgs.get(tile.getX()).get(tile.getY());
        highlight.setImage(image);
        highlight.setVisible(true);
    }

    /**
     * Asettaa ImageView:n x- ja z-koordinaatin
     * Tassa z-koordinaatti vastaa muissa luokissa kaytettya y-koordinaattia
     *
     * @param img ImageView, jonka koordinaatteja asetetaan
     * @param x   x-koordinaatti
     * @param z   z-koordinaatti
     */
    private void setImageXZ(ImageView img, int x, int z) {
        img.setTranslateX(x * TILE_LENGTH - 4 * TILE_LENGTH);
        img.setTranslateZ(z * TILE_LENGTH - 3.5 * TILE_LENGTH);
        img.setVisible(true);
    }


    // metodit kameran ja valojen alustamiseen

    private Camera initCamera() {
        PerspectiveCamera camera = new PerspectiveCamera(true);
        camera.setFarClip(200);

        camera.getTransforms().addAll(
                new Translate(0, -110, -70),
                new Rotate(-59, Rotate.X_AXIS)
        );

        return camera;
    }

    private Group initLights() {
        AmbientLight ambientLight = new AmbientLight(Color.WHITE);
        PointLight pointLight = new PointLight(Color.valueOf("#555555"));
        pointLight.setTranslateX(-6 * 12);

        return new Group(ambientLight, pointLight);
    }


    /**
     * Hakee tilen paalle tulevan ImageView:n, jota voidaan kayttaa highlight-kuvana
     *
     * @param texture naytettava kuva
     * @param yOffset y suunnan muutos kaannettyna, eli positiivinen yOffset tarkoittaa, etta kuva tulee
     *                taman verran korkeammalle laudan pinnasta, vaikka y-akseli on alaspain positiivinen.
     * @return uuden ImageView:n
     */
    private ImageView getImage3d(String texture, double yOffset) {
        ImageView img = new ImageView();
        if (texture != null) img.setImage(new Image(texture));

        img.setRotationAxis(new Point3D(1, 0, 0));
        img.setRotate(-90);
        img.setLayoutY(-3.03 - yOffset);
        img.setVisible(false);

        img.setFitWidth(TILE_LENGTH);
        img.setFitHeight(TILE_LENGTH);
        img.setMouseTransparent(true);

        return img;
    }

    private ImageView getImage3d(String texture) {
        return getImage3d(texture, 0);
    }


    /**
     * Kutsutaan aina kun jotain nappainta painetaan. Hallitaan siis nappainyhdistelmat.
     *
     * @param event tapahtuman KeyEvent
     */
    @FXML
    private void onKeyPressed(KeyEvent event) {

        // Yritetaan palata edelliseen tilaan, kun painetaan ctrl + Z.
        // Ei saa myoskaan raahata nappulaa samaan aikaan kun siirretaan nappulaa,
        // silla muuten olisi mahdollista skipata oma vuoro.
        if (event.isControlDown() && event.getCode() == KeyCode.Z && !onDrag) undo();
    }


    /**
     * Kaantaa lautaa 180 astetta
     */
    @FXML
    private void rotateBoard() {
        boardRotation.play();
    }


    @FXML
    private void undo() {
        if (allowUndo.isSelected()) {
            board.undo();
            setSelected(null);
        }
    }


    @FXML
    private void exit() {
        ((Stage) root.getScene().getWindow()).close();
    }


    private void newGame(boolean ai) {
        this.ai = ai;

        group3d.setRotate(0);

        board.reset();
        setSelected(null);
    }

    @FXML
    private void newGameNoAi() {
        newGame(false);
    }

    @FXML
    private void newGameWhite() {
        newGame(true);
    }

    @FXML
    private void newGameBlack() {
        newGame(true);
        makeAiMove();
    }

    @FXML
    private void saveGame() {
        Preferences pref = Preferences.userRoot().node("/chess");
        String value = board.save();
        pref.put("save", value);
        setSelected(null);
    }

    @FXML
    private void loadGame() {
        Preferences pref = Preferences.userRoot().node("/chess");
        String value = pref.get("save", "");
        if (value.isEmpty()) return;

        board.load(value);
        setSelected(null);
    }
}
