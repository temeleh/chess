# Chess Interface

JavaFX chess gui with 3d graphics and a simple minmax AI.

The tablet and chess pieces are originally from turbosquid.com with royalty free licences (all extented uses). The chess pieces are not available on turbosquid anymore, [but the tablet is (at least in 2022)](https://www.turbosquid.com/FullPreview/Index.cfm/ID/1097451).
The chess clock is my own 3d model made with Blender.

I started this project in 2017 and it is still incomplete. It is unlikely that I will ever continue this project.

![img](img.jpg)
